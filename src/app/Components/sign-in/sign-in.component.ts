import { Component, OnInit } from '@angular/core';
import { LoginModel } from 'src/app/Models/login.model';
import { AccountService } from 'src/app/auth/Services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private accountService:AccountService,private router:Router) { }

  invalidLogin:boolean=false;
  invalidPassword:boolean=false;
  credentials:LoginModel=new LoginModel();
    ngOnInit() {
  
    }
    login(){
      this.accountService.Login(this.credentials)
        .subscribe(res=>{
          if(res)
          this.router.navigate([""]);
        },error=>{
          this.invalidLogin=true;
        });
    ;
    }
    onNoClick(){
      this.credentials=new LoginModel();
    }
    register(){
      this.accountService.Register(this.credentials)
      .subscribe(res=>{
        if(res)
        this.router.navigate([""]);
      }
       ,error=>{
        this.invalidPassword=true;
       } ) ;
  
    }
  
  }
  
