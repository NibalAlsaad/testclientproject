import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { ProductModel } from 'src/app/Models/product.model';
import { ActivatedRoute } from '@angular/router';
import { ProductImagesModel } from 'src/app/Models/product-images.model';
import { API } from 'src/app/APIConfig';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private dataService:DataService,private route:ActivatedRoute) { }
 selectedProduct:ProductModel=new ProductModel();
 images:ProductImagesModel[]=[];
 imgUrl=API.img
  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.dataService.GetProduct(params.get('id'))
      .subscribe(res=>{
        this.selectedProduct=res;
        this.images=this.selectedProduct.productImages;
      })
    })
  
  }

}
