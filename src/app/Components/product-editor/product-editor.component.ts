import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/Services/data.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductModel } from 'src/app/Models/product.model';
import { API } from 'src/app/APIConfig';

@Component({
  selector: 'app-product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.css']
})
export class ProductEditorComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private dataService:DataService,
    public dialogRef: MatDialogRef<ProductEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    )
     {
      
     }
    selectedProductId:string;
    selectedProduct:ProductModel=new ProductModel();
    operationType:string;

    Products:ProductModel[]=[];
    theFile: any = null;
    messages: string[] = [];
    productImages:string[]=[];
    imgUrl=API.img
    ngOnInit(){
      this.operationType=this.data.operationType
             if(this.operationType=='Edit')
             {
              this.selectedProductId=this.data.id
               this.dataService
                 .GetProduct(this.selectedProductId)
                    .subscribe(res=>{
                      this.selectedProduct=res;
                    })
                      }
                  }
  uploadFile(event){
      let fileList: FileList = event.target.files;
      if(fileList.length > 0) {
          let file: File = fileList[0];
          let formData:FormData = new FormData();
          formData.append(file.name, file);

      this.dataService.upload(formData)
              .subscribe(res=>{
                console.log('Upload Result',res)
                this.productImages.push(res);
              })
          }
  }
                  
onSave(ProductModel:ProductModel){
  if(this.operationType=="Add"){
    this.selectedProduct.setImages=this.productImages;
  this.dataService.AddProduct(ProductModel)
  .subscribe(res=>{
    if(res.code==0){
      this.router.navigate(["Product"]);
       this.dialogRef.close();
    }
   
  });
}
  else  if(this.operationType=="Edit"){
  this.dataService.EditProduct(ProductModel)
  .subscribe(res=>{
    this.dialogRef.close();
  });
}

}

onNoClick(): void {
  this.dialogRef.close();
}
}
