import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ProductModel } from 'src/app/Models/product.model';
import { ProductEditorComponent } from '../product-editor/product-editor.component';
import { API } from 'src/app/APIConfig';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

 
  constructor(
    private dataService:DataService,
    private router:Router,
    public dialog: MatDialog )
     { }
 
  Products:ProductModel[]=[];
  displayedColumns: string[] = ['Name', 'Edit'];
  token=localStorage.getItem('token');
  isAdmin:boolean=false;
  imgUrl=API.img
  ngOnInit() {
    if(this.token && this.token.length>0){
      this.isAdmin=true;
    }
     this.dataService.GetAllProducts()
     .subscribe(res=>this.Products=res);
   }
   onDelete(ProductModel:ProductModel){
   this.dataService.DeleteProduct(ProductModel)
   .subscribe(
     ()=>{
      this.Products.splice(this.Products.findIndex((s)=>{return s.id==ProductModel.id}),1)
    }
   )
 }
 openAddDialog(): void {
  const dialogRef = this.dialog.open(ProductEditorComponent, {
    width: '700px',
    data: {operationType:'Add'}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    this.ngOnInit();
  });
}
openEditDialog(id): void {
  const dialogRef = this.dialog.open(ProductEditorComponent, {
    width: '700px',
    data: {operationType:'Edit',id:id}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    this.ngOnInit();
  });
}
 }