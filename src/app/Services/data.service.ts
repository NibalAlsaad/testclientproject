import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../APIConfig';
import { HeaderOptions } from './header-options';
import { ProductModel } from '../Models/product.model';
import { ProductResponse } from '../Models/Responses/productResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }


  //Products apis
  GetAllProducts():Observable< ProductModel[]>{
    return this.http.get< ProductModel[]>(API.url+"/Product/Products")
    ;
  }
  GetProduct(id:string):Observable< ProductModel>{
    return this.http.get< ProductModel>(API.url+"/Product/"+id);
  }
  AddProduct(ProductModel: ProductModel):Observable< ProductResponse>{
    return this.http.post< ProductResponse>(API.url+"/Product/Add",ProductModel,HeaderOptions);
  }
  EditProduct(ProductModel: ProductModel):Observable<ProductModel[]>{
    return this.http.put<ProductModel[]>(API.url+"/Product/Edit",ProductModel,HeaderOptions);
  }
  DeleteProduct(ProductModel: ProductModel):Observable<ProductModel[]>{
    return this.http.post<ProductModel[]>(API.url+"/Product/Delete",ProductModel,HeaderOptions);
  }
 upload(file):Observable<string>{
  let Header ={
    headers: new HttpHeaders({
       
        "Accept": "application/json"
      })
}
 return  this.http.post<string>(API.url+"/Upload",file,Header);
 }
}