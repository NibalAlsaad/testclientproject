import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { MaterialModule } from './material/material.module';
import { ToastComponent } from './Components/toast/toast.component';
import { ProductComponent } from './Components/product/product.component';
import { ProductDetailsComponent } from './Components/product-details/product-details.component';
import { ProductEditorComponent } from './Components/product-editor/product-editor.component';
import { SignInComponent } from './Components/sign-in/sign-in.component';
import { ProductViewComponent } from './Components/product-view/product-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ToastComponent,
    ProductComponent,
    ProductDetailsComponent,
    ProductEditorComponent,
    SignInComponent,
    ProductViewComponent  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,                            
    ReactiveFormsModule ,  
    HttpClientModule,
    MaterialModule,
    RouterModule.forRoot([
      { path:'product',component:ProductComponent},
      { path:'productview',component:ProductViewComponent},
      { path:'productEditor/:type',component:ProductEditorComponent},
      { path:'productDetails/:id',component:ProductDetailsComponent},
      { path:'login',component:SignInComponent},
      { path:'',component:ProductViewComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
