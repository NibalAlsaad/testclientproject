import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AccountService } from '../Services/account.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private accountService:AccountService,private router:Router) { }

  canActivate(){
   if(this.accountService.isLoggedIn()) 
   return true;
  this.router.navigate(["login"]);
  return false;
  }
}