import { Injectable } from '@angular/core';
import { LoginModel } from 'src/app/Models/login.model';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { API } from 'src/app/APIConfig';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http:HttpClient) {
  }

 Login(credentials:LoginModel):Observable<boolean>
 {
   return this.http.post(API.url+"/login",JSON.stringify(credentials) ,
   {headers:new HttpHeaders({"Content-Type": "application/json"})})
   .pipe(map(response=>{
     let result=<any>response;
     if(result && result.token){
       localStorage.setItem('token',result.token);
       return true;
   }
   return false;
   }));
 }
 Register(credentials:LoginModel):Observable<boolean>{
   return this.http.post(API.url+"/register",JSON.stringify(credentials) ,
   {headers:new HttpHeaders({"Content-Type": "application/json"})})
   .pipe(map(response=>{
     let result=<any>response;
     if(result && result.token){
       localStorage.setItem('token',result.token);
       return true;
     }
     return false;
   }));
 }
 LogOut(){
   localStorage.removeItem('token');
 }
 isLoggedIn(){
 //  let jwtHelper=new JwtHelper();
   let token=localStorage.getItem('token');
   if(!token)
   return false;
  // let isExipred=jwtHelper.isTokenExpired(token);
  // return !isExipred;

 }
}

