import { ProductImagesModel } from '../product-images.model';
export interface ProductImagesResponse{
code:number,
data:ProductImagesModel[]
}