import {ProductImagesModel} from './product-images.model'
export class ProductModel{

constructor() {
    this.productImages=[]
    this.setImages=[]
    
}
    public id :string  
    public title :string  
    public description :string  
    public price :number  
    public quantity  :number  
    public productImages :ProductImagesModel[]  
    public setImages:string  []
}